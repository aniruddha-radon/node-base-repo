import { Router } from "express";


export class Route {
    private static registeredPaths: string[] = [];
    constructor(
        public path: string,
        public router: Router
    ) { 
        // {path: "/user", router: router}
        if(!path.startsWith("/")) throw "INVALID PATH";

        if(Route.registeredPaths.includes(this.path)) throw "PATH ALREADY REGISTERED";

        Route.registeredPaths.push(this.path);
    }
}

export type Routes = Route[];