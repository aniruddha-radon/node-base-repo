import { NextFunction, Request, Response } from "express";
import { Schema } from "zod";

const validator = (
    source: "body" | "query" | "params", schema: Schema
) => (req: Request, res: Response, next: NextFunction) => {
    try {
        req[source] = schema.parse(req[source])
        next()
    } catch (e) {
        next({ statusCode: 400, message: "BAD REQUEST", errors: e });
    }
}

const body = (schema: Schema) => validator("body", schema);
const query = (schema: Schema) => validator("query", schema);
const params = (schema: Schema) => validator("params", schema);

export default {
    body,
    query,
    params
}